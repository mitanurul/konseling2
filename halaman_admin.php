<?php
session_start();
if(!isset($_SESSION['id_type'])) {
   header('location:login.php'); 
} else { 
   $email = $_SESSION['level']; 
}
?>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->


<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>KONSELING EBS</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="css1/agency.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css1/video.css">
  <link rel="stylesheet" type="text/css" href="css1/NAV.css">
  <link rel="stylesheet" href="css1/linearicons.css">
  <link rel="stylesheet" href="css1/footer.css">
  <link rel="stylesheet" href="css1/NAV.css">
  
  
		<link rel="stylesheet" href="css1/owl.carousel.css">
		<link rel="stylesheet" href="css1/font-awesome.min.css">
		<link rel="stylesheet" href="css1/nice-select.css">
		<link rel="stylesheet" href="css1/magnific-popup.css">
		<link rel="stylesheet" href="css1/bootstrap.css">
		<link rel="stylesheet" href="css1/main.css">
		<link rel="stylesheet" href="css1/VIDEO.css">
		
</head>

<body id="page-top">

  <!-- Navigation -->

<!------ Include the above in your HEAD tag ---------->

<nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-DARK FIXED-TOP">
  <a><img src="img/Logo1.png" width="60 px"></a>
  
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">
          <i class="fa fa-home"></i>
          Home
          <span class="sr-only">(current)</span>
          </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="CHAT.html">
           <i class="fa fa-envelope">
            <span class="badge badge-danger">11</span>
          </i>
          messege
        </a>
      </li>
      
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         
		  <i class="fa fa-book">
            <span class="badge badge-primary">5</span>
          </i>
          Article
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="CellOn-colorlib/pendidikan.html">Pendidikan</a>
          <a class="dropdown-item" href="CellOn-colorlib/bisnis.html">Bisnis</a>
		  <a class="dropdown-item" href="CellOn-colorlib/startup.html">Startup</a>
		  <a class="dropdown-item" href="CellOn-colorlib/spiritual.html">Spiritual</a></a>
		  <a class="dropdown-item" href="CellOn-colorlib/pengembangan.html">Pengembangan diri</a>
          <div class="dropdown-divider"></div>
          
        </div>
      </li>
	  
	  
	  
	  
    </ul>
	

    <ul class="navbar-nav ">
      
      <li class="nav-item">
        <a class="nav-link" href="#">
          <i class="fa fa-bell">
            <span class="badge badge-success">11</span>
          </i>
          Notifikasi
        </a>
      </li>
		
		
		<li class="nav-item ">
			<a class="nav-link href="#" id="navbarDropdown">
			<i class="fa fa-info"></i>
				about
			</a>
			
		</li>
		
		<li class="nav-item dropdown">
        <a class="nav-link" dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         
         
           <i class="fa fa-user"></i> 
          </i>
          user
        </a>
		 <div class="dropdown-menu" aria-labelledby="navbarDropdown">
			<a class="dropdown-item" href="profile_admin.php">profile</a>
			<a class="dropdown-item" href="logout.php">logout</a>
         
          <div class="dropdown-divider"></div>
          
        </div>
		
          
        </div>
      </li>
	  </ul>
	  
   

 
	  
	  
	
	  
	  
	  
	  
	  </div>
</nav>


  <!-- Header -->
  <header>
    <div class="overlay"></div>
  <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
    <source src="http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4" type="video/mp4">
  </video>
  <div class="container h-100">
    <div class="d-flex text-center h-100">
      <div class="my-auto w-100 text-white">
        <h1 class="display-3"></h1>
        <h2></h2>
      </div>
    </div>
  </div>
  </header>

  <!-- Services -->
  <section id="services">
   
     <img src="img/alur.png" width =1500px>
   
  </section>

  <!-- Portfolio Grid -->
  <section class="bg-light" id="portfolio">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">ARTIKEL</h2>
          <h3 class="section-subheading text-muted">favorite artikel minggu ini</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4 col-sm-6 portfolio-item">
          <a class="portfolio-link" data-toggle="modal" href="#portfolioModal1">
            <div class="portfolio-hover">
              <div class="portfolio-hover-content">
                <i class="ionicons ion-ios-book fa-3x"></i>
              </div>
            </div>
            <div class="image">
			<span>
			<img src="https://sleekr.co/wp-content/uploads/2019/03/shutterstock_186761918-min.jpg" width="350 px"></span>
			</div>
          </a>
          <div class="portfolio-caption">
            <h4>Langkah Penting dan Strategi HRD dalam Menghadapi Revolusi Industri 4.0</h4>
            <p>Masa yang baru telah tiba dan sudah berada di depan mata</p>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-item">
          <a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
            <div class="portfolio-hover">
              <div class="portfolio-hover-content">
                <i class="ionicons ion-ios-book fa-3x"></i>
              </div>
            </div>
            <div class="image">
								<span>
								<img src="https://s3-ap-southeast-1.amazonaws.com/swa.co.id/wp-content/uploads/2015/04/2015-04-08_21.12.301.jpg" width="350 px"></span>
			</div>
			</a>
          <div class="portfolio-caption">
            <h4>Seluk Beluk Perjalanan Go-Jek Menjadi Startup Unicorn</h4>
            <p>siapa yang tak kenal nama ini. Berawal dari startup (perusahaan rintisan) kecil,
									</p>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-item">
          <a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
            <div class="portfolio-hover">
              <div class="portfolio-hover-content">
                <i class="ionicons ion-ios-book fa-3x"></i>
              </div>
            </div>
           <div class="image">
								<span>
								<img src="https://cdn.moneysmart.id/wp-content/uploads/2018/11/22120516/shutterstock_1149152354-min-700x497.jpg" width="350 px"></span>
							</div>
			</a>
          <div class="portfolio-caption">
            <h4>Tokopedia, Startup E-Commerce Yang Mampu Bertahan Dan Terus Bertumbuh</h4>
								</h4>
            <p>Semenjak hype startup tahun 2009/2010 lalu...............</p>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-item">
          <a class="portfolio-link" data-toggle="modal" href="#portfolioModal4">
            <div class="portfolio-hover">
              <div class="portfolio-hover-content">
               <i class="ionicons ion-ios-book fa-3x"></i>
              </div>
			 
            </div>
             <div class="image">
								<span>
								<img src="https://image.cermati.com/q_70/jhxjpa0mdtyls7zj0ajy.webp" width="350 px"></span>
							</div>
          </a>
          <div class="portfolio-caption">
            <h4>17 Startup Sukses Milik Generasi Millenial Indonesia ini Punya 5 Tips yang Bisa Ditiru</h4>
								</h4>
            <p class="text-muted">Tak hanya ide bisnis yang kuat tapi pelajari juga tips-tips cerdas dari startup lain yang sudah sukses.</p>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-item">
          <a class="portfolio-link" data-toggle="modal" href="#portfolioModal5">
            <div class="portfolio-hover">
              <div class="portfolio-hover-content">
                <i class="ionicons ion-ios-book fa-3x"></i>
              </div>
			 
            </div>
			 <div class="image">
								<span>
								<img src="https://d26bwjyd9l0e3m.cloudfront.net/wp-content/uploads/2019/02/Group-High-Five-Featured.jpg" width="350 px"></span>
							</div>
           </a>
          <div class="portfolio-caption">
            <h4>Pola dari Semua Kisah Sukses Startup yang Patut Kamu Ketahui</h4>
								</h4>
            <p class="text-muted">Masa depan adalah milik orang-orang yang menceritakan kisah terbaik. Di balik setiap kisah yang menarik terdapat pola universal
</p>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-item">
          <a class="portfolio-link" data-toggle="modal" href="#portfolioModal6">
            <div class="portfolio-hover">
              <div class="portfolio-hover-content">
                <i class="ionicons ion-ios-book fa-3x"></i>
              </div>
            </div>
            <div class="image">
			<span>
				<img src="https://kissflow.com/wp-content/uploads/2015/10/Accounting-for-Startups-is-often-more-about-the-People.png" width="350 px"></span>
			</div>
          </a>
          <div class="portfolio-caption">
            <h4>Ini 7 Rahasia Sukses Mengembangkan Bisnis Startup</h4>
								</h4>
            <p class="text-muted">startup yang sedang kamu kembangkan bisa berjalan lancar kok, asalkan, kamu bisa menjalani 7 langkah berikut, dengan baik
			</p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- About -->
  <section id="about">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">KATEGORI</h2>
          <h3 class="section-subheading text-muted">Yukk kenali kategori konseling ESQBS</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <ul class="timeline">
            <li>
              <div class="timeline-image">
                <img class="rounded-circle img-fluid" src="img/bisnis3.png" alt="">
              </div>
              <div class="timeline-panel">
                <div class="timeline-heading">
                  <h3>BISNIS</h3>
                  <h4 class="subheading"> HI sobat enterpreneur! banyak ide bisnis yang bisa kamu bangun loh, banyak hal kecil yang bisa menambah incomemu loh,  yukkk hubungi konselor yang ahli di bidangnya  
							</h4>
                </div>
                <div class="timeline-body">
                  <p class="text-muted"></p>
                </div>
              </div>
            </li>
            <li class="timeline-inverted">
              <div class="timeline-image">
                <img class="rounded-circle img-fluid" src="img/bisnis2.png" alt="">
              </div>
              <div class="timeline-panel">
                <div class="timeline-heading">
                  <h3>STARTUP</h3>
                  <h4 class="subheading">kamu mahasiswa? pengen punya penghasilan? atau pengen merealisasikan idemu? bingung mulainya dari mana, yuuk konsul!kamu akan di bimbing oleh konelor yang ahli di bidangnya</h4>
                </div>
                <div class="timeline-body">
                  <p class="text-muted"></P></div>
              </div>
            </li>
            <li>
              <div class="timeline-image">
                <img class="rounded-circle img-fluid" src="img/edu1.png" alt="">
              </div>
              <div class="timeline-panel">
                <div class="timeline-heading">
                  <h3>Pendidikan</h3>
                  <h4 class="subheading">di sini kamu bisa mendapatkan informasi tentang sistem manajemen pengelolaan cara belajar yang benar dan  efektif. kamu juga bisa berdiskusi untuk ilmu ilmu yang sedang kamu dalami 
							</h4>
                </div>
                <div class="timeline-body">
                  <p class="text-muted"></p></div>
              </div>
            </li>
            <li class="timeline-inverted">
              <div class="timeline-image">
                <img class="rounded-circle img-fluid" src="img/SPIRITUAL.png" alt="">
              </div>
              <div class="timeline-panel">
                <div class="timeline-heading">
                  <h4>SPIRITUAL</h4>
                  <h4 class="subheading">HI Ukhsist dan Brosist! Merasa dir futur dan lalai? atau kamu haus akan ilmu agama, yukk konsultasikan pada ahlinya. 
							</h4>
                </div>
                <div class="timeline-body">
                  <p class="text-muted"></p></div>
              </div>
            </li>
			 <li>
              <div class="timeline-image">
                <img class="rounded-circle img-fluid" src="img/karakter1.png" alt="">
              </div>
              <div class="timeline-panel">
                <div class="timeline-heading">
                  <h3>Pendidikan</h3>
                  <h4 class="subheading">Bosan dengan hidup kamu yang super flat, bosan dengan ptensimu yang gitu gitu aja? kamu seperti hanrus menghubungi konselor, yukkk diskusi dan temukan potensimu
							</h4>
                </div>
                <div class="timeline-body">
                  <p class="text-muted"></p></div>
              </div>
            </li>
            
          </ul>
        </div>
      </div>
    </div>
  </section>

  <!-- Team -->
  <section class="bg-light" id="team">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Our Amazing Konselor</h2>
          
        </div>
      </div>
      <div class="row">
        <div class="col-sm-4">
          <div class="team-member">
            <img class="mx-auto rounded-circle" src="img/DESTA.jpg" alt="">
            <h4>Desta S S.Kom, M.Kom </h4>
            <p class="text-muted"><h5>Head of Computer Science</h5></p>
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-facebook-f"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-linkedin-in"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="team-member">
            <img class="mx-auto rounded-circle" src="img/lely.jpg" alt="">
            <h4>Leli Deswindi S.E., MT</h4>
            <p class="text-muted"><h5>Head of Business Management</h5></p>
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-facebook-f"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-linkedin-in"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="team-member">
            <img class="mx-auto rounded-circle" src="img/ERZA.jpg" alt="">
            <h4>Erza Sofian S.Kom, M.Sc</h4>
            <p class="text-muted"><h5>Lecture</h5></p>
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-facebook-f"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-linkedin-in"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
		<div class="col-sm-4">
          <div class="team-member">
            <img class="mx-auto rounded-circle" src="img/anggar.jpg" alt="">
            <h4>Anggar Riskinanto, ST., MTI</h4>
            <p class="text-muted"><h5>Lecture</h5></p>
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-facebook-f"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-linkedin-in"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
		<div class="col-sm-4">
          <div class="team-member">
            <img class="mx-auto rounded-circle" src="img/bayu.jpg" alt="">
            <h4>Bayu k S.Kom., M.Kom</h4>
            <p class="text-muted"><h5>Lecture</h5></p>
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-facebook-f"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-linkedin-in"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
		
		
		<div class="col-sm-4">
          <div class="team-member">
            <img class="mx-auto rounded-circle" src="img/Barir.jpg" alt="">
            <h4>Abdul Barir S.Kom, MTI</h4>
            <p class="text-muted"><h5>Lecture</h5></p>
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-facebook-f"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-linkedin-in"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
		
      </div>
      <div class="row">
        <div class="col-lg-8 mx-auto text-center">
          <p class="large text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut eaque, laboriosam veritatis, quos non quis ad perspiciatis, totam corporis ea, alias ut unde.</p>
        </div>
      </div>
    </div>
  </section>

  <!-- Clients -->
 <!-- <section class="py-5">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-6">
          <a href="#">
            <img class="img-fluid d-block mx-auto" src="img/logos/envato.jpg" alt="">
          </a>
        </div>
        <div class="col-md-3 col-sm-6">
          <a href="#">
            <img class="img-fluid d-block mx-auto" src="img/logos/designmodo.jpg" alt="">
          </a>
        </div>
        <div class="col-md-3 col-sm-6">
          <a href="#">
            <img class="img-fluid d-block mx-auto" src="img/logos/themeforest.jpg" alt="">
          </a>
        </div>
        <div class="col-md-3 col-sm-6">
          <a href="#">
            <img class="img-fluid d-block mx-auto" src="img/logos/creative-market.jpg" alt="">
          </a>
        </div>
      </div>
    </div>
  </section>-->

  <!-- Contact -->
  <section id="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Kritik dan saran</h2>
          <h3 class="section-subheading text-muted">Contoc us</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <form id="contactForm" name="sentMessage" novalidate="novalidate">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <input class="form-control" id="name" type="text" placeholder="Your Name *" required="required" data-validation-required-message="Please enter your name.">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <input class="form-control" id="email" type="email" placeholder="Your Email *" required="required" data-validation-required-message="Please enter your email address.">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <input class="form-control" id="phone" type="tel" placeholder="Your Phone *" required="required" data-validation-required-message="Please enter your phone number.">
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <textarea class="form-control" id="message" placeholder="Your Message *" required="required" data-validation-required-message="Please enter a message."></textarea>
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <div class="clearfix"></div>
              <div class="col-lg-12 text-center">
                <div id="success"></div>
                <button id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase" type="submit">Send Message</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>

  <!-- Footer -->
  <footer class="section-gap">
			<div class="container">
				<div class="row pt-60">
					<div class="col-lg-5 col-sm-6">
						<div class="single-footer-widget">
						<h2><strong>Lokasi</strong>
						<a href="https://www.google.com/maps/d/u/0/embed?mid=1zzJvU_Tkju1lNQMH2Oo2LYWUlKQ&ie=UTF8&hl=en&msa=0&ll=-6.290864999999976%2C106.80940399999997&spn=0%2C0&t=h&output=embed&z=17"><img class="img-fluid d-flex mx-auto" src="img/map.png" width= "800 px" alt=""></a>
							</img>
							
						</div>
					</div>
					
					<div class="col-lg-2 col-sm-6">
						
					</div>
					
					
					<div class="col-lg-5 col-sm-6">
						<div class="single-footer-widget">
							<h6 class="text-uppercase mb-20">Quick About</h6>
							<p>
								Sistem konseling yang di rancang khusus untuk mahasiswa ESQ Business School dengan para konselor yang ahli di bidangnya
							</p>
							<p>
								+00 012 6325 98 6542 <br>
							
							</p>
							<div class="footer-social d-flex align-items-center">
								<a href="#"><i class="fa fa-facebook"></i></a>
								<a href="#"><i class="fa fa-twitter"></i></a>
								<a href="#"><i class="fa fa-dribbble"></i></a>
								<a href="#"><i class="fa fa-behance"></i></a>
							</div>
						</div>
					</div>
				</div>
				<div class="footer-bottom d-flex justify-content-between align-items-center flex-wrap">
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
					<p class="footer-text m-0">Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by konseling team</a></p>
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
				</div>
			</div>
		</footer>
  <!-- Portfolio Modals -->

  <!-- Modal 1 -->
  <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
          <div class="lr">
            <div class="rl"></div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <div class="modal-body">
                <!-- Project Details Go Here -->
                <h2 class="text-uppercase">Langkah Penting dan Strategi HRD dalam Menghadapi Revolusi Industri 4.0
								 </h2>
                <p class="item-intro text-muted">Masa yang baru telah tiba dan sudah berada di depan mata</p>
                <img class="img-fluid d-block mx-auto" src="https://sleekr.co/wp-content/uploads/2019/03/shutterstock_186761918-min.jpg" alt="">
                <p>Masa yang baru telah tiba dan sudah berada di depan mata. Revolusi Industri 4.0, yang terbentuk karena semakin pesatnya perkembangan teknologi akan membawa manusia pada masa dimana semua akan semakin otomatis. Untuk tetap bisa mengikuti perkembangan zaman ini, setiap bagian dan aspek dari perusahaan perlu melakukan adaptasi. HRD sebagai divisi pengelolaan perlu membentuk strategi HRD baru yang lebih mutakhir.

Perkembangan teknologi yang merubah setiap aspek kegiatan di perusahaan secara masif akan membawa banyak peluang pekerjaan baru. Hal ini disebabkan oleh semakin terhubungnya setiap orang di dunia, sehingga peningkatan besar akan terjadi di sisi efektivitas bisnis. Berbagai disiplin perusahaan, termasuk HRD, akan terpengaruh dan harus benar-benar memahami perubahan ini.<ul class="list-inline">
                  </p><li>Date: January 2017</li>
                  <li>Client: Threads</li>
                  <li>Category: Illustration</li>
                </ul>
				<button class="btn btn-primary" type="button">
                <a href="https://sleekr.co/blog/strategi-hrd-jitu-menghadapi-revolusi-industri-4-0/"  ><h5> Read more.....</h5></a>
				</button>
			  </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal 2 -->
  <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
          <div class="lr">
            <div class="rl"></div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <div class="modal-body">
                <!-- Project Details Go Here -->
                <h2 class="text-uppercase">Project Name</h2>
                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                <img class="img-fluid d-block mx-auto" src="img/portfolio/02-full.jpg" alt="">
                <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                <ul class="list-inline">
                  <li>Date: January 2017</li>
                  <li>Client: Explore</li>
                  <li>Category: Graphic Design</li>
                </ul>
                <button class="btn btn-primary" data-dismiss="modal" type="button">
                  <i class="fas fa-times"></i>
                  Close Project</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal 3 -->
  <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
          <div class="lr">
            <div class="rl"></div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <div class="modal-body">
                <!-- Project Details Go Here -->
                <h2 class="text-uppercase">Project Name</h2>
                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                <img class="img-fluid d-block mx-auto" src="img/portfolio/03-full.jpg" alt="">
                <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                <ul class="list-inline">
                  <li>Date: January 2017</li>
                  <li>Client: Finish</li>
                  <li>Category: Identity</li>
                </ul>
                <button class="btn btn-primary" data-dismiss="modal" type="button">
                  <i class="fas fa-times"></i>
                  Close Project</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal 4 -->
  <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
          <div class="lr">
            <div class="rl"></div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <div class="modal-body">
                <!-- Project Details Go Here -->
                <h2 class="text-uppercase">Project Name</h2>
                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                <img class="img-fluid d-block mx-auto" src="img/portfolio/04-full.jpg" alt="">
                <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                <ul class="list-inline">
                  <li>Date: January 2017</li>
                  <li>Client: Lines</li>
                  <li>Category: Branding</li>
                </ul>
                <button class="btn btn-primary" data-dismiss="modal" type="button">
                  <i class="fas fa-times"></i>
                  Close Project</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal 5 -->
  <div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
          <div class="lr">
            <div class="rl"></div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <div class="modal-body">
                <!-- Project Details Go Here -->
                <h2 class="text-uppercase">Project Name</h2>
                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                <img class="img-fluid d-block mx-auto" src="img/portfolio/05-full.jpg" alt="">
                <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                <ul class="list-inline">
                  <li>Date: January 2017</li>
                  <li>Client: Southwest</li>
                  <li>Category: Website Design</li>
                </ul>
                <button class="btn btn-primary" data-dismiss="modal" type="button">
                  <i class="fas fa-times"></i>
                  Close Project</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal 6 -->
  <div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
          <div class="lr">
            <div class="rl"></div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <div class="modal-body">
                <!-- Project Details Go Here -->
                <h2 class="text-uppercase">Project Name</h2>
                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                <img class="img-fluid d-block mx-auto" src="img/portfolio/06-full.jpg" alt="">
                <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                <ul class="list-inline">
                  <li>Date: January 2017</li>
                  <li>Client: Window</li>
                  <li>Category: Photography</li>
                </ul>
                <button class="btn btn-primary"  type="button">
                  <i class="fas fa-times"></i>
                  read more....</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Contact form JavaScript -->
  <script src="js1/jqBootstrapValidation.js"></script>
  <script src="js1/contact_me.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js1/agency.min.js"></script>

</body>

</html>
