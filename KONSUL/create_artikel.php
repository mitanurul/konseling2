<?if($_SESSION['id_type']=="1"){
		header("location:login.php?pesan=gagal");
	}
 
	?>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Register</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.css">
    <link rel="stylesheet" href="css/owl.transitions.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="css/normalize.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="css/main.css">
    <!-- morrisjs CSS
		============================================ -->
    <link rel="stylesheet" href="css/morrisjs/morris.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- metisMenu CSS
		============================================ -->
    <link rel="stylesheet" href="css/metisMenu/metisMenu.min.css">
    <link rel="stylesheet" href="css/metisMenu/metisMenu-vertical.css">
    <!-- calendar CSS
		============================================ -->
    <link rel="stylesheet" href="css/calendar/fullcalendar.min.css">
    <link rel="stylesheet" href="css/calendar/fullcalendar.print.min.css">
    <!-- forms CSS
		============================================ -->
    <link rel="stylesheet" href="css/form/all-type-forms.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <div class="color-line"></div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="back-link back-backend">
                    <a href="dashboard_admin.html" class="btn btn-primary">Back to Dashboard</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"></div>
            <div class="col-md-6 col-md-6 col-sm-6 col-xs-12">
                <div class="text-center custom-login">
                    <h3>Registration</h3>
                    <p></p>
                </div>
                <div class="hpanel">
                    <div class="panel-body">
                        <form action="peroses_artikel.php" method="post" id="loginForm">
                            <div class="row">
								
								<div class="form-group col-lg-12">
										<label>Foto</br>Pilih File Gambar</label>
										<input class="form-control" type="file" name="image" placeholder="nama" required="">
								</div>
								
								<div name="email"class="form-group col-lg-12">
                                    <label>jenis artikel</label>
									<select name="jenis_artikel" class="form-control pro-edt-select form-control-primary">
																
																<option value="1">Bisnis</option>
																<option value="2">Pendidikan</option>
																<option value="3">spiritual</option>
																<option value="3">pengembangan_diri</option>
									</select>
								   </div>
								   <div class="form-group col-lg-12">
										<label>judul</label>
										<input class="form-control" type="text" name="judul" placeholder="nama" required="">
									</div>
								<div class="form-group col-lg-12">
                                    <label>Isi Artikel</label>
                                    <textarea  class="form-control" type="text" name="isi" ></textarea><br>
								</div>
								<div class="form-group col-lg-12">
                                    <label>tanggal post</label>
                                    <textarea  class="form-control" type="date" name="tgl_pos" ></textarea><br>
								</div>
								<div class="form-group col-lg-12">
									<label>id konselor</label>
									 <select name="id_konselor" class="form-control pro-edt-select form-control-primary">
																
																<option value="1">1</option>
																<option value="2">2</option>
													
									</select>
								</div>
								<div class="form-group col-lg-12">
									<label>id kategori</label>
									 <select name="id_kategori" class="form-control pro-edt-select form-control-primary">
																
																<option value="1">1</option>
																<option value="2">2</option>
																<option value="3">3</option>
									</select>
								</div>
								
								
                            <div class="text-center">
                                <button class="btn btn-success loginbtn">Submit</button>
                                <button class="btn btn-default">Cancel</button>
                            </div>
                        </form>
						
						 <?php

    // Check If form submitted, insert form data into users table.
    if(isset($_POST['Submit'])) {
        $name = $_POST['image'];
        $jenis_artikel = $_POST['jenis_artikel'];
        $judul = $_POST['judul'];
		$isi	= $_POST['isi'];
		$tgl_pos = $_post['tgl_pos'];
		$id_konselor = $_POST['id_konselor'];
		$id_kategori = $_POST['id_kategori'];
		
        // include database connection file
        include_once("koneksi.php");

        // Insert user data into table
        $result = mysqli_query($koneksi, "INSERT INTO artikel(image, jenis_artikel, judul, isi, tgl_pos, id_konselor, id_kategori) VALUES ('$image', '$jenis_artikel', '$judul', '$isi', '$tgl_pos', '$id_konselor', '$id_kategori')");

        // Show message when user added
        echo "Artikel added successfully. <a href='daftar_artikel.php'>View Users</a>";
    }
    ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"></div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <p>Copyright © 2018 <a href="https://colorlib.com/wp/templates/">Colorlib</a> All rights reserved.</p>
            </div>
        </div>
    </div>

    <!-- jquery
		============================================ -->
    <script src="js/vendor/jquery-1.11.3.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="js/jquery-price-slider.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="js/jquery.meanmenu.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="js/owl.carousel.min.js"></script>
    <!-- sticky JS
		============================================ -->
    <script src="js/jquery.sticky.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="js/jquery.scrollUp.min.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="js/scrollbar/mCustomScrollbar-active.js"></script>
    <!-- metisMenu JS
		============================================ -->
    <script src="js/metisMenu/metisMenu.min.js"></script>
    <script src="js/metisMenu/metisMenu-active.js"></script>
    <!-- tab JS
		============================================ -->
    <script src="js/tab.js"></script>
    <!-- icheck JS
		============================================ -->
    <script src="js/icheck/icheck.min.js"></script>
    <script src="js/icheck/icheck-active.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="js/plugins.js"></script>
    <!-- main JS
		============================================ -->
    <script src="js/main.js"></script>
</body>

</html>