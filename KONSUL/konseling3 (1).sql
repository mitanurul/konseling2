-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 16, 2019 at 05:35 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `konseling3`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(8) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `jenis_kelamin` varchar(1) NOT NULL,
  `no_kontak` varchar(15) NOT NULL,
  `id_regis` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `nama`, `alamat`, `jenis_kelamin`, `no_kontak`, `id_regis`) VALUES
(1, 'Utha', 'Jeruk Purut, Jakarta Selatan', 'L', '081567898702', 3);

-- --------------------------------------------------------

--
-- Table structure for table `artikel`
--

CREATE TABLE `artikel` (
  `id_artikel` int(8) NOT NULL,
  `image` blob NOT NULL,
  `jenis_artikel` varchar(50) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `isi` varchar(255) NOT NULL,
  `tgl_pos` date NOT NULL,
  `id_konselor` int(8) NOT NULL,
  `id_kategori` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikel`
--

INSERT INTO `artikel` (`id_artikel`, `image`, `jenis_artikel`, `judul`, `isi`, `tgl_pos`, `id_konselor`, `id_kategori`) VALUES
(1, '', 'Bisnis', 'Mengembangkan Bisnis', 'Mengembangkan Bisnis dengan sungguh-sungguh', '2019-01-05', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id_chat` int(8) NOT NULL,
  `isi` varchar(255) NOT NULL,
  `waktu_chat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dari` varchar(50) NOT NULL,
  `ke` varchar(50) NOT NULL,
  `id_konseling` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`id_chat`, `isi`, `waktu_chat`, `dari`, `ke`, `id_konseling`) VALUES
(1, 'Assalaamu\'alaikum', '2019-05-13 04:24:18', 'Utha@gmail.com', 'Risna@gmail.com', 1),
(2, 'Wa\'alaikum salaam', '2019-05-13 17:20:40', 'Risna@gmail.com', 'Utha@gmail.com', 1),
(5, 'Saya ingin konseling', '2019-05-13 18:00:00', 'Utha@gmail.com', 'Risna@gmail.com', 1),
(6, 'Perihal apa ya??', '2019-05-14 18:21:00', 'Risna@gmail.com', 'Utha@gmail.com', 1),
(7, 'Tentang cita-cita saya bu', '2019-05-14 04:00:00', 'Utha@gmail.com', 'Risna@gmail.com', 1),
(11, 'Entah', '2019-05-16 19:31:24', 'Utha@gmail.com', 'Risna@gmail.com', 1),
(14, 'mit', '2019-05-16 19:41:19', 'Utha@gmail.com', 'mita@gmail.com', 1),
(15, 'dmn', '2019-05-16 19:41:48', 'Utha@gmail.com', 'mita@gmail.com', 1),
(16, 'di kos mau kesini? ahhhh', '2019-05-16 19:45:36', 'mita@gmail.com', 'Utha@gmail.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(8) NOT NULL,
  `jenis_kategori` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `jenis_kategori`) VALUES
(1, 'Bisnis'),
(2, 'Karakter');

-- --------------------------------------------------------

--
-- Table structure for table `konseling`
--

CREATE TABLE `konseling` (
  `id_konseling` int(8) NOT NULL,
  `status` varchar(35) NOT NULL,
  `id_konselor` int(8) NOT NULL,
  `id_kategori` int(8) NOT NULL,
  `nim` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konseling`
--

INSERT INTO `konseling` (`id_konseling`, `status`, `id_konselor`, `id_kategori`, `nim`) VALUES
(1, 'Aktif', 1, 2, 1),
(2, 'Tidak Aktif', 2, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `konselor`
--

CREATE TABLE `konselor` (
  `id_konselor` int(8) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `jenis_kelamin` varchar(1) NOT NULL,
  `no_kontak` varchar(15) NOT NULL,
  `id_regis` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konselor`
--

INSERT INTO `konselor` (`id_konselor`, `nama`, `alamat`, `jenis_kelamin`, `no_kontak`, `id_regis`) VALUES
(1, 'Venny', 'Cilandak, jakarta Selatan', 'P', '08234567890', 2),
(2, 'Sujoko winanto', 'Jakarta Barat', 'L', '0876789067', 4);

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `nim` int(8) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `prodi` varchar(35) NOT NULL,
  `no_kontak` varchar(15) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `jenis_kelamin` varchar(1) NOT NULL,
  `semester` int(2) NOT NULL,
  `id_regis` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`nim`, `nama`, `prodi`, `no_kontak`, `alamat`, `jenis_kelamin`, `semester`, `id_regis`) VALUES
(1, 'Aidil', 'Ilmu Komputer', '086780987654', 'Jeruk Purut, Jakarta Selatan', 'L', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `registrasi`
--

CREATE TABLE `registrasi` (
  `id_regis` int(8) NOT NULL,
  `email` varchar(35) NOT NULL,
  `password` varchar(8) NOT NULL,
  `id_type` int(8) NOT NULL,
  `nama` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registrasi`
--

INSERT INTO `registrasi` (`id_regis`, `email`, `password`, `id_type`, `nama`) VALUES
(1, 'aidil@gmail.com', '123456', 3, 'Aidil'),
(2, 'venny@gmail.com', '456789', 2, 'Venny'),
(3, 'utha@gmail.com', '1234567', 1, 'utha'),
(4, 'Sujoko Winanto', '789789', 2, 'sujoko'),
(7, 'm.nurul.y@students.esqbs.id', '0987', 1, 'Mita'),
(9, 'admin@app.com', '123456', 1, 'Manda'),
(10, 'mitanurulyatimah98@gmail.com', '1234567', 1, 'MITA NURUL'),
(11, 'mahasiswa@gmail.com', '123456', 1, 'mahasiswa'),
(12, 'ima@gmail.com', '123456', 1, 'Ima'),
(14, 'nurul@gmail.com', '123456', 1, 'nurul'),
(15, 'Najmi@gmail.com', '123456', 2, 'Manda'),
(16, 'Rahma@gmail.com', '123456', 1, 'rahma');

-- --------------------------------------------------------

--
-- Table structure for table `reqtiket`
--

CREATE TABLE `reqtiket` (
  `id_reqTiket` int(8) NOT NULL,
  `proritas` varchar(15) NOT NULL,
  `jadwal` date NOT NULL,
  `status` varchar(15) NOT NULL,
  `id_konseling` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reqtiket`
--

INSERT INTO `reqtiket` (`id_reqTiket`, `proritas`, `jadwal`, `status`, `id_konseling`) VALUES
(2, 'Normal', '2019-01-07', 'Yes', 1);

-- --------------------------------------------------------

--
-- Table structure for table `riwayat`
--

CREATE TABLE `riwayat` (
  `id_riwayat` int(8) NOT NULL,
  `riwayat` varchar(255) NOT NULL,
  `id_konseling` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `riwayat`
--

INSERT INTO `riwayat` (`id_riwayat`, `riwayat`, `id_konseling`) VALUES
(1, 'Bisnis', 1),
(2, 'Karakter', 2);

-- --------------------------------------------------------

--
-- Table structure for table `type_user`
--

CREATE TABLE `type_user` (
  `id_type` int(8) NOT NULL,
  `jenis_user` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `type_user`
--

INSERT INTO `type_user` (`id_type`, `jenis_user`) VALUES
(1, 'Admin'),
(2, 'Konselor'),
(3, 'Mahasiswa');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `id_type` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `password`, `nama`, `id_type`) VALUES
('mita@gmail.com', '123456', 'Mita', 1),
('Risna@gmail.com', '123456', 'Risna', 2),
('Utha@gmail.com', '123456', 'Uta', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`),
  ADD KEY `id_regis` (`id_regis`);

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id_artikel`),
  ADD KEY `id_konselor` (`id_konselor`),
  ADD KEY `id_kategori` (`id_kategori`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id_chat`),
  ADD KEY `dari` (`dari`),
  ADD KEY `ke` (`ke`),
  ADD KEY `id_konseling` (`id_konseling`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `konseling`
--
ALTER TABLE `konseling`
  ADD PRIMARY KEY (`id_konseling`),
  ADD KEY `id_konselor` (`id_konselor`),
  ADD KEY `nim` (`nim`),
  ADD KEY `id_kategori` (`id_kategori`);

--
-- Indexes for table `konselor`
--
ALTER TABLE `konselor`
  ADD PRIMARY KEY (`id_konselor`),
  ADD KEY `id_regis` (`id_regis`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`nim`),
  ADD KEY `id_regis` (`id_regis`);

--
-- Indexes for table `registrasi`
--
ALTER TABLE `registrasi`
  ADD PRIMARY KEY (`id_regis`),
  ADD KEY `id_type` (`id_type`);

--
-- Indexes for table `reqtiket`
--
ALTER TABLE `reqtiket`
  ADD PRIMARY KEY (`id_reqTiket`),
  ADD KEY `id_konseling` (`id_konseling`);

--
-- Indexes for table `riwayat`
--
ALTER TABLE `riwayat`
  ADD PRIMARY KEY (`id_riwayat`),
  ADD KEY `id_konseling` (`id_konseling`);

--
-- Indexes for table `type_user`
--
ALTER TABLE `type_user`
  ADD PRIMARY KEY (`id_type`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `id_type` (`id_type`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `artikel`
--
ALTER TABLE `artikel`
  MODIFY `id_artikel` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id_chat` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `konseling`
--
ALTER TABLE `konseling`
  MODIFY `id_konseling` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `konselor`
--
ALTER TABLE `konselor`
  MODIFY `id_konselor` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `nim` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `registrasi`
--
ALTER TABLE `registrasi`
  MODIFY `id_regis` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `reqtiket`
--
ALTER TABLE `reqtiket`
  MODIFY `id_reqTiket` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `riwayat`
--
ALTER TABLE `riwayat`
  MODIFY `id_riwayat` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `type_user`
--
ALTER TABLE `type_user`
  MODIFY `id_type` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `admin_ibfk_1` FOREIGN KEY (`id_regis`) REFERENCES `registrasi` (`id_regis`);

--
-- Constraints for table `artikel`
--
ALTER TABLE `artikel`
  ADD CONSTRAINT `artikel_ibfk_1` FOREIGN KEY (`id_konselor`) REFERENCES `konselor` (`id_konselor`),
  ADD CONSTRAINT `artikel_ibfk_2` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id_kategori`);

--
-- Constraints for table `chat`
--
ALTER TABLE `chat`
  ADD CONSTRAINT `chat_ibfk_1` FOREIGN KEY (`dari`) REFERENCES `user` (`id_user`),
  ADD CONSTRAINT `chat_ibfk_2` FOREIGN KEY (`ke`) REFERENCES `user` (`id_user`),
  ADD CONSTRAINT `chat_ibfk_3` FOREIGN KEY (`id_konseling`) REFERENCES `konseling` (`id_konseling`);

--
-- Constraints for table `konseling`
--
ALTER TABLE `konseling`
  ADD CONSTRAINT `konseling_ibfk_1` FOREIGN KEY (`id_konselor`) REFERENCES `konselor` (`id_konselor`),
  ADD CONSTRAINT `konseling_ibfk_2` FOREIGN KEY (`nim`) REFERENCES `mahasiswa` (`nim`),
  ADD CONSTRAINT `konseling_ibfk_3` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id_kategori`);

--
-- Constraints for table `konselor`
--
ALTER TABLE `konselor`
  ADD CONSTRAINT `konselor_ibfk_1` FOREIGN KEY (`id_regis`) REFERENCES `registrasi` (`id_regis`);

--
-- Constraints for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD CONSTRAINT `mahasiswa_ibfk_1` FOREIGN KEY (`id_regis`) REFERENCES `registrasi` (`id_regis`);

--
-- Constraints for table `registrasi`
--
ALTER TABLE `registrasi`
  ADD CONSTRAINT `registrasi_ibfk_1` FOREIGN KEY (`id_type`) REFERENCES `type_user` (`id_type`);

--
-- Constraints for table `reqtiket`
--
ALTER TABLE `reqtiket`
  ADD CONSTRAINT `reqtiket_ibfk_1` FOREIGN KEY (`id_konseling`) REFERENCES `konseling` (`id_konseling`);

--
-- Constraints for table `riwayat`
--
ALTER TABLE `riwayat`
  ADD CONSTRAINT `riwayat_ibfk_1` FOREIGN KEY (`id_konseling`) REFERENCES `konseling` (`id_konseling`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id_type`) REFERENCES `type_user` (`id_type`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
