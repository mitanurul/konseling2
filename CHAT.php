<?php
include 'koneksi.php';
session_start();
$me = $_SESSION['email'];
if (isset($_GET['friends'])) {
    $friends = $_GET['friends'];
}
?>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html>
    <head>
        <title>Chat</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.js"></script>

        <link rel="stylesheet" href="CSS1/CHAT.css">

    </head>
    <body>
        <div class="container-fluid h-100">
            <div class="row justify-content-center h-100">
                <div class="col-md-4 col-xl-3 chat"><div class="card mb-sm-3 mb-md-0 contacts_card">
                        <div class="card-header">
                            
                            <div class="input-group">
                                
                                <button class="d-flex justify-content-start fas fa-arrow-left back_btn"></button>
                                <input type="text" placeholder="Search..." name="" class="form-control search">
                                <div class="input-group-prepend">
                                    <span class="input-group-text search_btn"><i class="fas fa-search"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="card-body contacts_body">
                            <ui class="contacts">
                                <?php
                                $all = mysqli_query($koneksi, "SELECT * FROM registrasi ORDER BY nama");
                                while ($u = mysqli_fetch_array($all)) {
                                    if ($u['email'] != $me) {
                                        ?>
                                        <a href="chat.php?friends=<?php echo $u['email']; ?>">
                                            <li>
                                                <div class="d-flex bd-highlight">
                                                    <div class="img_cont">
                                                        <img src="#" class="rounded-circle user_img">
                                                        <span class="online_icon offline"></span>
                                                    </div>
                                                    <div class="user_info">
                                                        <span><?php echo $u['nama']; ?></span>
                                                        <p><?php echo $u['nama']; ?> left 0 mins ago</p>
                                                    </div>
                                                </div>
                                            </li>
                                        </a>
                                        <?php
                                    }
                                }
                                ?>
                            </ui>
                        </div>
                        <div class="card-footer"></div>
                    </div></div>
                <div class="col-md-8 col-xl-6 chat">
                    <div class="card">

                        <?php
                        if (isset($_GET['friends'])) {
                            $chat = mysqli_query($koneksi, "SELECT * FROM chat ORDER BY waktu_chat");
                            $temp_friends = mysqli_query($koneksi, "SELECT * FROM registrasi WHERE email='$friends'");
                            $data_friends = mysqli_fetch_array($temp_friends);
                            $temp_me = mysqli_query($koneksi, "SELECT * FROM registrasi WHERE email='$me'");
                            $data_me = mysqli_fetch_array($temp_me);
							?>
                            <div class="card-header msg_head">
                                <div class="d-flex bd-highlight">
                                    <div class="img_cont">
                                        <img src="#" class="rounded-circle user_img">
                                        <span class="online_ikoneksi"></span>
                                    </div>
                                    <div class="user_info">
                                        <span>Chat with <?php echo $data_friends['nama']; ?></span>
                                        <p>0 Messages</p>
                                    </div>
                                </div>
                                <span id="action_menu_btn"><i class="fas fa-ellipsis-v"></i></span>
                                <div class="action_menu">
                                    <ul>
                                        <li><i class="fas fa-user-circle"></i> View profile</li>
                                        <li><i class="fas fa-users"></i> Add to close friends</li>
                                        <li><i class="fas fa-plus"></i> Add to group</li>
                                        <li><i class="fas fa-ban"></i> Block</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-body msg_card_body">
                                <?php
                                while ($chat_friends = mysqli_fetch_array($chat)) {
                                    $dt = $chat_friends['waktu_chat'];
                                    if (($chat_friends['dari'] == $data_friends['id_regis']) && ($chat_friends['ke'] == $data_me['id_regis'])) {
                                        ?>
                                        <div class="d-flex justify-content-start mb-4">
                                            <div class="img_cont_msg">
                                                <img src="#" class="rounded-circle user_img_msg">
                                            </div>
                                            <div class="msg_cotainer">
                                                <div><?php echo $chat_friends['isi']; ?></div>
                                                <div>
                                                <span class="msg_time_send fixed-bottom position-absolute"><?php echo date("d/m/Y H:i:s", strtotime($dt)); ?></span>
                                            </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if (($chat_friends['dari'] == $data_me['id_regis']) && ($chat_friends['ke'] == $data_friends['id_regis'])) { ?>
                                        <div class="d-flex justify-content-end mb-4">
                                            <div class="msg_cotainer_send">
                                                <div><?php echo $chat_friends['isi']; ?></div>
                                            <div>
                                                <span class="msg_time_send fixed-bottom position-absolute"><?php echo date("d/m/Y H:i:s", strtotime($dt)); ?></span>
                                            </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                            <div class="card-footer">
                                <form class="form-horizontal" method="post" action="save_chat.php?friends=<?php echo $friends ?>">
                                    <div class="input-group">
                                        <div class="input-group-append">
                                            <span class="input-group-text attach_btn"><i class="fas fa-paperclip"></i></span>
                                        </div>
                                        <textarea name="isi" class="form-control type_msg" placeholder="Type your message..."></textarea>
                                        <div class="input-group-append">
                                            <button class="input-group-text send_btn"><i class="fas fa-location-arrow"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script src="js1/CHAT.js"></script>
</html>